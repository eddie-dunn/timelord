import extend from 'bff/extend';
import eventEmitter from 'bff/event-emitter';

export default extend({}, eventEmitter);
