const SAVE_LIST_THROTTLE_TIME = 1000; // ms
const hasScheduledSaveList = {};

const loadList = key => JSON.parse(localStorage[key] || '[]');

const saveList = (key, list) => {
  if (hasScheduledSaveList[key]) {
    return;
  }
  hasScheduledSaveList[key] = true;
  setTimeout(() => {
    localStorage[key] = JSON.stringify(list);
    hasScheduledSaveList[key] = false;
  }, SAVE_LIST_THROTTLE_TIME);
};

const KEYS = {
  TASKS: 'tasks',
  TIMELINE_ITEMS: 'timeline-items',
  VERSION: 'version',
};

const localStorageToObject = () => {
  const data = {};
  const numKeys = localStorage.length;
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < numKeys; i++) {
    const key = localStorage.key(i);
    try {
      data[key] = JSON.parse(localStorage[key]);
    } catch (e) {
      console.warn(e, localStorage[key]);
    }
  }
  return data;
};

const objectToLocalStorage = objectData => {
  Object.getOwnPropertyNames(objectData).forEach(key => {
    localStorage[key] = JSON.stringify(objectData[key]);
  });
};

const migrate = currentData =>
  new Promise(resolve => {
    const version = currentData[KEYS.VERSION] || 1;
    // since the migration decides what next version will be, we can't fetch in paralell
    // eslint-disable-next-line no-await-in-loop
    System.import(`./migrations/${version}`)
      .then(
        migration => {
          if (!migration) return currentData;
          const nextData = migration.default(currentData);
          if (process.env.NODE_ENV === 'development') {
            //eslint-disable-next-line
            console.info(`migrating ${version} -> ${nextData[KEYS.VERSION]}`);
          }

          return migrate(nextData);
        },
        () => {
          if (process.env.NODE_ENV === 'development') {
            //eslint-disable-next-line
            console.info(`migration done ${version}`);
          }
          return currentData;
        },
      )
      .then(resolve);
  });

const migrateLocalStorage = () => {
  const initialVersion = localStorageToObject();

  if (process.env.NODE_ENV === 'development') {
    //eslint-disable-next-line
    console.info(`migrating from ${initialVersion[KEYS.VERSION]}`);
  }
  migrate(initialVersion).then(objectToLocalStorage);
};

export { loadList, saveList, KEYS, migrate, objectToLocalStorage, localStorageToObject, migrateLocalStorage };
