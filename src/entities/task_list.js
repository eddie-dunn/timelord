import List from 'bff/list';

const TaskList = List.withProperties({});

TaskList.prototype.isValidExportableId = function(taskId) {
  return Number.isInteger(taskId) && taskId > 0;
};

TaskList.prototype.isTaskIdUnique = function(taskId) {
  return this.every(task => task.id !== taskId);
};

TaskList.prototype.getNextUniqueId = function(isExportable, seed) {
  const getNextIdCandidate = id => id + (isExportable ? 1 : -1);
  let nextId = seed || (isExportable ? 0 : -1);
  while (!this.isTaskIdUnique(nextId)) {
    nextId = getNextIdCandidate(nextId);
  }
  return nextId;
};

export default TaskList;
