import Record from 'bff/record';

let serialNumber = 0;
const hashConstant = 0.5 * (Math.sqrt(5) - 1);
const maxAngle = 360;

const Task = Record.withProperties({
  id: {
    type: Number,
  },
  name: {
    type: String,
  },
  lastUsed: {
    type: Number,
    defaultValue: () => Date.now() - (serialNumber += 1),
  },
  isFavorite: {
    type: Boolean,
    defaultValue: false,
  },
  nReferences: {
    type: Number,
    defaultValue: 0,
  },
  colorAngle: {
    type: Number,
    setter: false,
    getter() {
      // Calculate a "hash" value in the range [0, maxAngle[
      // https://www.cs.hmc.edu/~geoff/classes/hmc.cs070.200101/homework10/hashfuncs.html
      const a = Math.abs(this.id) * hashConstant;
      const b = a % 1; // Get fractional part, range [0, 1[
      return Math.floor(b * maxAngle);
    },
  },
  isExportable: {
    type: Boolean,
    setter: false,
    getter() {
      return this.id >= 0;
    },
  },
});

Task.prototype.toJSON = function() {
  return {
    id: this.id,
    name: this.name,
    lastUsed: this.lastUsed,
    isFavorite: this.isFavorite,
  };
};

export default Task;
