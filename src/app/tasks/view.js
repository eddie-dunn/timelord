import extend from 'bff/extend';
import View from 'bff/view';

import { loadList, saveList, KEYS as localStorageKeys } from 'env/local_storage';
import Task from 'entities/task';

import AddTaskView from './add/view';
import TasksListView from './list/view';
import defaultTasks from './default_tasks.json';
import template from './template.dot';

export default View.makeSubclass({
  constructor(tasks) {
    this.tasks = tasks;

    this.render();

    const addTaskView = new AddTaskView(tasks);
    const taskListView = new TasksListView(tasks, addTaskView.inputState);

    this.$('.content-pane').appendChild(addTaskView.el);
    this.$('.content-pane').appendChild(taskListView.el);

    this.listenTo(tasks, ['change:length', 'item:change'], this.storeTasksLocally);

    this.loadLocallyStoredTasks();
  },

  template,

  storeTasksLocally() {
    saveList(localStorageKeys.TASKS, this.tasks);
  },

  loadLocallyStoredTasks() {
    const locallyStoredTasksData = loadList(localStorageKeys.TASKS);
    const tasksData = extend(locallyStoredTasksData, defaultTasks, 'useTarget');
    const tasks = tasksData.map(taskData => new Task(taskData));
    this.tasks.pushAll(tasks);
  },
});
