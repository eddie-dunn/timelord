import Record from 'bff/record';
import View from 'bff/view';

import time from 'env/time';
import vent from 'env/vent';

import template from './template.dot';

const itemMinStartTime = 0; // minutes since midnight
const itemMaxEndTime = 24 * 60; // minutes since midnight
const itemMinDuration = 5; // minutes

function clamp(val, min, max) {
  return Math.min(max, Math.max(val, min));
}

const State = Record.withProperties({
  isNew: { type: Boolean, defaultValue: true },
  isResizing: { type: Boolean, defaultValue: false },
  isFloating: { type: Boolean, defaultValue: false },
  isEditing: { type: Boolean, defaultValue: false },
  originalStartTime: { type: Number, defaultValue: 0 },
  originalEndTime: { type: Number, defaultValue: 0 },
  initialMouseY: { type: Number, defaultValue: 0 }, // Unit: em
  currentMouseY: { type: Number, defaultValue: 0 }, // Unit: em
  mouseDeltaY: {
    type: Number,
    setter: false,
    dependencies: ['initialMouseY', 'currentMouseY'],
    getter() {
      return this.currentMouseY - this.initialMouseY;
    },
  },
});

export default View.makeSubclass({
  constructor(item, items) {
    this.item = item;
    this.items = items;
    this.state = new State();

    this.render();

    this.setEditMode(this.state.isEditing);
    this.listenTo(this.items, 'change:length', this.requestRender);
    this.listenTo(this.item, 'change', this.requestRender);
    this.listenTo(this.item.task, 'change', this.triggerItemChange);
    this.listenTo(this.item, 'removed', this.destroy);
    this.listenTo(this.item, 'reordered', this.onReordered);
    this.listenTo(this.item, 'moved forward', this.onMovedForward);
    this.listenTo(this.state, 'change', this.requestRender);
    this.listenTo(this.state, 'change:isEditing', this.setEditMode);

    this.listenTo(this.el, 'animationend', this.onAnimationEnd);
    this.listenTo(this.el, 'mouseenter', this.requestRender);
  },

  pxToEm(em) {
    // TODO: move this to BFF.View
    const fontSize = parseFloat(getComputedStyle(this.el).fontSize);
    return em / fontSize;
  },

  onAnimationEnd(ev) {
    if (ev.animationName === 'createAnim') {
      this.state.isNew = false;
      this.stopListening(this.el, 'animationend');
    }
  },

  triggerItemChange() {
    this.item.emit('change'); // TODO: Make BFF to handle this automatically
  },

  setEditMode(isEditing, wasEditing) {
    this.stopListening(this.el);
    this.stopListening(document.body);
    this.stopListening('*');
    if (isEditing) {
      this.listenTo('.actions .delete', 'click', this.onDeleteClicked);
      this.listenTo('.actions .add-end', 'click', this.onAddEndClicked);
      this.listenTo(document.body, 'click', this.onEditingClick);
      this.listenTo(document.body, 'keydown', this.onEditingKeydown);
    } else {
      this.listenTo(this.el, 'dblclick', this.onDoubleClick);
      this.listenTo(this.el, 'mousedown', this.onMouseDown);
      this.listenTo('.expand-to-now', 'click', this.expandToNow);
      if (wasEditing) {
        this.item.description = this.$('.description').textContent;
      }
    }
  },

  template,

  time,

  render() {
    View.prototype.render.call(this);
    if (this.state.isEditing) {
      this.$('.description').focus();
    }
  },

  onDoubleClick() {
    this.state.isEditing = true;
  },

  onEditingClick(ev) {
    if (!this.el.contains(ev.target)) {
      this.state.isEditing = false;
    }
  },

  onEditingKeydown(ev) {
    switch (ev.key) {
      case 'Escape':
      case 'Enter':
        ev.preventDefault();
        this.state.isEditing = false;
        break;
    }
  },

  onDeleteClicked() {
    this.state.isEditing = false;
    this.listenTo(this.el, 'transitionend', () => {
      this.listenTo(this.el, 'transitionend', () => {
        this.item.emit('request delete self', this.item);
      });
      this.item.endTime = this.item.startTime;
    });
  },

  onAddEndClicked() {
    this.state.isEditing = false;
    this.listenTo(this.el, 'transitionend', () => {
      this.stopListening(this.el, 'transitionend');
      this.item.task.lastUsed = Date.now();
      vent.emit('request add task to timeline', this.item.task);
    });
  },

  expandToNow() {
    this.item.endTime = time.snap(time.getMinutesSinceMidnight());
  },

  onMouseDown(ev) {
    if (ev.button !== 0) {
      return;
    }

    ev.preventDefault();
    ev.stopImmediatePropagation();

    this.state.initialMouseY = this.pxToEm(ev.clientY);
    this.state.currentMouseY = this.pxToEm(ev.clientY);

    if (ev.target === this.$('.start.time-handle')) {
      this.onStartTimeDragStart();
    } else if (ev.target === this.$('.end.time-handle')) {
      this.onEndTimeDragStart();
    } else if (this.items.length === 1) {
      this.onMoveDragStart();
    } else {
      this.onReorderDragStart();
    }

    this.listenTo(document.body, 'mousemove', mouseEv => {
      this.state.currentMouseY = this.pxToEm(mouseEv.clientY);
    });
    this.listenTo(document.body, 'mouseup', this.onMouseUp);
  },

  onMouseUp() {
    this.stopListening(document.body);
    this.stopListening(this.state, 'change:currentMouseY');
    this.state.isResizing = false;
    this.state.isFloating = false;
    this.state.initialMouseY = 0;
    this.state.currentMouseY = 0;
  },

  onStartTimeDragStart() {
    this.state.isResizing = true;
    this.state.originalStartTime = this.item.startTime;
    this.listenTo(this.state, 'change:currentMouseY', this.onStartTimeDragUpdate);
  },

  onStartTimeDragUpdate(mouseY) {
    const startTimeOffset = (mouseY - this.state.initialMouseY) * time.emToMinutes;
    const newStartTime = time.snap(this.state.originalStartTime + startTimeOffset);
    this.item.startTime = clamp(newStartTime, itemMinStartTime, this.item.endTime - itemMinDuration);
  },

  onEndTimeDragStart() {
    this.state.isResizing = true;
    this.state.originalEndTime = this.item.endTime;
    this.listenTo(this.state, 'change:currentMouseY', this.onEndTimeDragUpdate);
  },

  onEndTimeDragUpdate(mouseY) {
    const endTimeOffset = (mouseY - this.state.initialMouseY) * time.emToMinutes;
    const newEndTime = time.snap(this.state.originalEndTime + endTimeOffset);
    this.item.endTime = clamp(newEndTime, this.item.startTime + itemMinDuration, itemMaxEndTime);
  },

  onReorderDragStart() {
    this.listenTo(this.state, 'change:currentMouseY', this.onReorderDragUpdate);
  },

  onReorderDragUpdate(mouseY) {
    if (!this.state.isFloating) {
      this.state.isFloating = true;
      this.state.originalStartTime = this.item.startTime;
    }
    const timeOffset = (mouseY - this.state.initialMouseY) * time.emToMinutes;
    this.item.emit('reorder update', timeOffset, this.item);
  },

  onReordered(startTimeDelta) {
    this.state.initialMouseY = this.state.initialMouseY - startTimeDelta * time.minutesToEm;
  },

  onMoveDragStart() {
    this.state.originalStartTime = this.item.startTime;
    this.state.originalEndTime = this.item.endTime;
    this.listenTo(this.state, 'change:currentMouseY', this.onMoveDragUpdate);
  },

  onMoveDragUpdate(mouseY) {
    this.onStartTimeDragUpdate(mouseY);
    this.onEndTimeDragUpdate(mouseY);
  },

  onMovedForward() {
    setTimeout(() => {
      if (this.el.nextSibling) {
        // Sibling might have gone away during timeout
        this.el.parentNode.insertBefore(this.el.nextSibling, this.el);
      }
    }, 120); // Wait longer than the animation
  },
});
