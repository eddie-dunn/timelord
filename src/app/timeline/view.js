import Record from 'bff/record';
import View from 'bff/view';

import { loadList, saveList, KEYS as localStorageKeys } from 'env/local_storage';
import TimelineItem from 'entities/timeline_item';
import ExportView from 'app/export/view';

import TrackView from './track/view';
import FooterView from './footer/view';
import template from './template.dot';

const State = Record.withProperties({
  itemsBackup: { type: Array, defaultValue: [] },
  isZooming: { type: Boolean, defaultValue: false },
  zoom: { type: Number, defaultValue: 1 },
});

export default View.makeSubclass({
  constructor(tasks, items) {
    this.tasks = tasks;
    this.items = items;
    this.state = new State();

    this.render();

    this.$('.content-pane').appendChild(new TrackView(items, this.state).el);
    this.$('.content-pane').appendChild(new FooterView(items, this.state).el);

    this.listenTo(items, ['change:length', 'item:change'], this.storeItemsLocally);
    this.listenTo(items, 'change:length', this.onItemsLengthChanged);
    this.listenTo(items, 'item:added', this.onItemAdded);
    this.listenTo(items, 'item:removed', this.onItemRemoved);
    this.listenTo(items, 'change', this.requestRender);
    this.listenTo(this.state, 'change', this.requestRender);
    this.listenTo('button.clear', 'click', this.clearTimelineItems);
    this.listenTo('button.undo', 'click', this.undoClearTimelineItems);

    this.listenTo('button.export', 'click', () => {
      new ExportView(items); // eslint-disable-line no-new
    });

    this.loadLocallyStoredItems(tasks);
  },

  template,

  storeItemsLocally() {
    saveList(localStorageKeys.TIMELINE_ITEMS, this.items);
  },

  loadLocallyStoredItems() {
    const itemsData = loadList(localStorageKeys.TIMELINE_ITEMS);
    const items = this.transformLocalStorageDataToEntityData(itemsData);
    this.items.pushAll(items);
  },

  transformLocalStorageDataToEntityData(localStorageData) {
    return localStorageData.map(itemData => {
      const task = this.tasks.find(t => t.id === itemData.taskId);
      const { taskId, ...rest } = itemData;
      return new TimelineItem({
        ...rest,
        task,
      });
    });
  },

  clearTimelineItems() {
    if (this.items.length === 0) {
      return;
    }
    this.state.itemsBackup = loadList(localStorageKeys.TIMELINE_ITEMS);
    this.items.clear();
  },

  onItemsLengthChanged(length, prevLength) {
    if (prevLength === 0) {
      this.state.itemsBackup = [];
    }
  },

  undoClearTimelineItems() {
    const items = this.transformLocalStorageDataToEntityData(this.state.itemsBackup);
    this.items.pushAll(items);
  },

  onItemAdded(item) {
    // eslint-disable-next-line no-param-reassign
    item.task.nReferences += 1;
  },

  onItemRemoved(item) {
    // eslint-disable-next-line no-param-reassign
    item.task.nReferences -= 1;
  },
});
