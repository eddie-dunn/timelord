import groupBy from 'lodash/fp/groupBy';
import filter from 'lodash/fp/filter';
import flatten from 'lodash/fp/flatten';
import reduce from 'lodash/fp/reduce';
import sortBy from 'lodash/fp/sortBy';
import findIndex from 'lodash/fp/findIndex';
import time from 'env/time';
import map from 'lodash/fp/map';
import flow from 'lodash/fp/flow';

export const exportItemFromTimelineItem = timelineItem => ({
  id: timelineItem.taskId.toString(),
  duration: timelineItem.duration,
  formatedDuration: '',
  description: timelineItem.description || timelineItem.task.name,
  startTime: timelineItem.startTime,
});

export default flow(
  filter(item => item.task.isExportable),
  groupBy('taskId'),
  map(items =>
    reduce((accumulator, item) => {
      if (!accumulator.length || item.description) {
        const newItem = exportItemFromTimelineItem(item);
        const indexExisting = findIndex({ description: newItem.description })(accumulator);

        if (indexExisting === -1) {
          return [newItem, ...accumulator];
        }

        const existingItem = accumulator[indexExisting];
        return [
          {
            ...existingItem,
            duration: existingItem.duration + newItem.duration,
          },
          ...accumulator.slice(0, indexExisting),
          ...accumulator.slice(indexExisting + 1),
        ];
      }

      const [lastAdded, ...rest] = accumulator;
      return [
        {
          ...lastAdded,
          duration: lastAdded.duration + item.duration,
        },
        ...rest,
      ];
    }, [])(items),
  ),
  flatten,
  sortBy(item => item.startTime),
  map(item => ({
    ...item,
    formatedDuration: time.minutesToTimeString(item.duration),
  })),
);
