import map from 'lodash/fp/map';
import reduce from 'lodash/fp/reduce';
import flow from 'lodash/fp/flow';

import timelineItemsToAggregatedExportItems from '../export-items';

const tasks = [
  {
    id: 1,
    name: 'task 1',
    isExportable: true,
  },
  {
    id: 2,
    name: 'task 2',
    isExportable: true,
  },
];

describe('timeline export', () => {
  it('Should calculate exported time correctly', () => {
    it('for single task in timeline', () => {
      const timelineItems = [
        {
          task: tasks[0],
          taskId: 1,
          startTime: 0,
          endTime: 10,
          duration: 10,
        },
        {
          task: tasks[0],
          taskId: 1,
          startTime: 10,
          endTime: 20,
          duration: 10,
          description: '1',
        },
        {
          task: tasks[0],
          taskId: 1,
          startTime: 20,
          endTime: 30,
          duration: 10,
          description: '2',
        },
        {
          task: tasks[0],
          taskId: 1,
          startTime: 40,
          endTime: 50,
          duration: 10,
          description: '1',
        },
      ];

      const totalExportedDuration = flow(
        map(item => item.duration),
        reduce((accumulator, item) => accumulator + item, 0),
      )(timelineItemsToAggregatedExportItems(timelineItems));
      expect(totalExportedDuration).toEqual(40);
    });
    it('for multiple tasks in timeline', () => {
      const timelineItems = [
        {
          task: tasks[0],
          taskId: 1,
          startTime: 0,
          endTime: 10,
          duration: 10,
        },
        {
          task: tasks[0],
          taskId: 1,
          startTime: 10,
          endTime: 20,
          duration: 10,
          description: '1',
        },
        {
          task: tasks[1],
          taskId: 2,
          startTime: 20,
          endTime: 30,
          duration: 10,
        },
        {
          task: tasks[1],
          taskId: 2,
          startTime: 40,
          endTime: 50,
          duration: 10,
          description: '2',
        },
        {
          task: tasks[0],
          taskId: 1,
          startTime: 50,
          endTime: 60,
          duration: 10,
          description: '1',
        },
        {
          task: tasks[1],
          taskId: 2,
          startTime: 60,
          endTime: 70,
          duration: 10,
          description: '2',
        },
      ];

      const totalExportedDuration = flow(
        map(item => item.duration),
        reduce((accumulator, item) => accumulator + item, 0),
      )(timelineItemsToAggregatedExportItems(timelineItems));
      expect(totalExportedDuration).toEqual(60);
    });
  });
});
