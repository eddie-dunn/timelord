import View from 'bff/view';

import TaskList from 'entities/task_list';
import TimelineItemList from 'entities/timeline_item_list';

import TimelineView from './timeline/view';
import TasksView from './tasks/view';
import template from './template.dot';

export default View.makeSubclass({
  constructor() {
    this.render();

    const tasks = new TaskList();
    const items = new TimelineItemList();

    const tasksView = new TasksView(tasks, items);
    const timelineView = new TimelineView(tasks, items);

    this.el.appendChild(tasksView.el);
    this.el.appendChild(timelineView.el);
  },

  template,
});
