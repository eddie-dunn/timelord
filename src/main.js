import AppView from 'app/view';
import vent from 'env/vent';
import { migrateLocalStorage } from 'env/local_storage';

migrateLocalStorage();
document.body.appendChild(new AppView().el);

// Trigger global "app started" event when the app has been created, attached to the DOM
// and all resources has been loaded, processed and rendered.
const onLoaded = vent.emit.bind(vent, 'app started');
if (document.readyState === 'complete') {
  onLoaded();
} else {
  window.addEventListener(
    'load',
    () => {
      window.removeEventListener('load', onLoaded, false);
      onLoaded();
    },
    false,
  );
}
