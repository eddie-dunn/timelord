/* eslint-disable */
import webpack from 'webpack';
import webpackConfigs from './webpack.config';

/**
 * Creates application bundles from the source files.
 */
function bundle() {
  return Promise.all(webpackConfigs.map(webpackConfig => new Promise((resolve, reject) => {
    const compiler = webpack(webpackConfig);
    const handler = (err, stats) => {
      if (err) {
        return reject(err);
      }

      console.log(stats.toString(webpackConfig.stats));
      return resolve();
    };
    if (process.argv.includes('--watch')) {
      compiler.watch({}, handler);
    } else {
      compiler.run(handler);
    }
  })));
}

export default bundle;
