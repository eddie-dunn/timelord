/* eslint-disable */
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import webpackConfigs from './webpack.config';

const PORT = 3000;
const DOMAIN = 'localhost';
const HOST = `${DOMAIN}:${PORT}`;
const ORIGIN = `http://${HOST}`;
const HREF = `${ORIGIN}/index-dev.html`;

function bundle() {
/**
 * Creates application bundles from the source files.
 */
  return Promise.all(webpackConfigs.map(webpackConfig => new Promise(() => {
    webpackConfig.entry.client = [`webpack-dev-server/client?${ORIGIN}`, ...webpackConfig.entry.client];
    const compiler = webpack(webpackConfig);
    const server = new WebpackDevServer(compiler, {
        // webpack-dev-server options

      contentBase: './',
        // Can also be an array, or: contentBase: "http://localhost/",

      hot: false,
        // Enable special support for Hot Module Replacement
        // Page is no longer updated, but a "webpackHotUpdate" message is sent to the content
        // Use "webpack/hot/dev-server" as additional module in your entry point
        // Note: this does _not_ add the `HotModuleReplacementPlugin` like the CLI option does.

      compress: false,

      clientLogLevel: 'info',
        // Control the console log messages shown in the browser when using inline mode.
        // Can be `error`, `warning`, `info` or `none`.

        // webpack-dev-middleware options
      quiet: false,
      noInfo: false,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
      },
        // It's a required option.
      publicPath: '/dist/',
      stats: { colors: true },

    });
    server.listen(PORT, DOMAIN, () => {});
    console.log(HREF);
  })));
}

export default bundle;
