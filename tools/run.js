/* eslint-disable */
function run(functionOrModule, options) {
  const callable = functionOrModule.default ? functionOrModule.default : functionOrModule;
  return callable(options);
}

if (require.main === module && process.argv.length > 2) {
  delete require.cache[__filename];
  const module = require(`./${process.argv[2]}.js`).default;
  run(module).catch((err) => { console.error(err.stack); process.exit(1); });
}

export default run;
