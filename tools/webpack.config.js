import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import pkg from '../package.json';

const isDebug = !process.argv.includes('--release');
const isVerbose = process.argv.includes('--verbose');


const extractStyle = new ExtractTextPlugin({
  filename: 'main.css',
  disable: isDebug,
});

const config = {
  context: path.resolve(__dirname, '../src'),

  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: 'dist/',
    pathinfo: isVerbose,
  },

  module: {
    rules: [
      {
        test: /\.dot$/,
        loader: 'dot-loader',
      },
      {
        test: /\.scss$/,
        loader: extractStyle.extract({
          fallback: {
            loader: 'style-loader',
            options: {
              sourceMap: isDebug,
            },
          },
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: isDebug,
                importLoaders: 1,
                minimize: !isDebug,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: isDebug,
                includePaths: [
                  path.resolve(__dirname, '../node_modules/normalize-css'),
                ],
              },
            },
          ],
        }),
      },
    ],
  },

  resolve: {
    modules: [path.resolve(__dirname, '../src'), 'node_modules'],
    alias: {
      doT: 'dot',
      bff: `bff-lib/dist/${isDebug ? 'dev' : 'prod'}`,
      normalize: 'normalize-css/normalize.css',
    },
  },

  resolveLoader: {
    alias: {
      text: 'raw-loader',
    },
  },

  // Don't attempt to continue if there are any errors.
  bail: !isDebug,

  cache: isDebug,

  stats: {
    colors: true,
    reasons: isDebug,
    hash: isVerbose,
    version: isVerbose,
    timings: true,
    chunks: isVerbose,
    chunkModules: isVerbose,
    cached: isVerbose,
    cachedAssets: isVerbose,
  },
};

//
// Configuration for the client-side bundle (client.js)
// -----------------------------------------------------------------------------

const clientConfigES5 = {
  ...config,

  name: 'client',
  target: 'web',

  entry: {
    client: ['./main.js', './main.scss'],
  },

  output: {
    ...config.output,
    filename: '[name].js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include: [
          path.resolve(__dirname, '../src'),
        ],
        query: {
          // https://github.com/babel/babel-loader#options
          cacheDirectory: isDebug,

          // https://babeljs.io/docs/usage/options/
          babelrc: false,
          presets: [
            // A Babel preset that can automatically determine the Babel plugins and polyfills
            // https://github.com/babel/babel-preset-env
            ['env', {
              targets: {
                browsers: pkg.browserslist,
              },
              modules: false,
              useBuiltIns: false,
              debug: false,
            }],
            // Experimental ECMAScript proposals
            // https://babeljs.io/docs/plugins/#presets-stage-x-experimental-presets-
            'stage-2',
          ],
          plugins: [], //TODO
        },
      },
      ...config.module.rules,
    ],
  },

  resolve: { ...config.resolve },

  plugins: [
    // Define free variables
    // https://webpack.github.io/docs/list-of-plugins.html#defineplugin
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': isDebug ? '"development"' : '"production"',
      'process.env.BROWSER': true,
      __DEV__: isDebug,
    }),

     // Move modules that occur in multiple entry chunks to a new entry chunk (the commons chunk).
    // http://webpack.github.io/docs/list-of-plugins.html#commonschunkplugin
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: module => /node_modules/.test(module.resource),
    }),

    ...isDebug ? [] : [
      // Minimize all JavaScript output of chunks
      // https://github.com/mishoo/UglifyJS2#compressor-options
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
          screw_ie8: true,
          warnings: isVerbose,
          unused: true,
          dead_code: true,
        },
        mangle: {
          screw_ie8: true,
        },
        output: {
          comments: false,
          screw_ie8: true,
        },
      }),
    ],

    extractStyle,

    ...isDebug ? [new webpack.HotModuleReplacementPlugin()] : [],
  ],

  // Choose a developer tool to enhance debugging
  // http://webpack.github.io/docs/configuration.html#devtool
  devtool: isDebug ? 'cheap-module-source-map' : false,

  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  // https://webpack.github.io/docs/configuration.html#node
  // https://github.com/webpack/node-libs-browser/tree/master/mock
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },
};

export default [clientConfigES5];
